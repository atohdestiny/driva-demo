using UnityEngine;
using UnityEngine.UI;
using Quota.SceneManagement;
using System;

public class RequestManager : MonoBehaviour
{

	[SerializeField] private InputField Name;
	[SerializeField] private InputField Location;

	[SerializeField] private InputField Destination;
	[SerializeField] private InputField DateAndTime;

	[SerializeField] private InputField Luggages;
	[SerializeField] private InputField Number;

	[SerializeField] private InputField Tel;
	private const string reciepient_EmailAddress = "destinyatoh@gmail.com";

	[SerializeField] private Text controllerText;

	private string message;
	private SceneChanger sceneChanger;

	private void Awake()
	{
		sceneChanger = FindObjectOfType<SceneChanger>();
	}//Awake

	private void Update()
	{
		//do a check to make sure only the right data types are input
		if(Name.text.Contains("0") || Name.text.Contains("1") || Name.text.Contains("2") ||
			Name.text.Contains("3") || Name.text.Contains("4") || Name.text.Contains("5") ||
			Name.text.Contains("6") || Name.text.Contains("7") || Name.text.Contains("8") ||
			Name.text.Contains("9") || Name.text.Contains(".") || Name.text.Contains(",") || Name.text.Contains("/") ||
			Name.text.Contains("?") || Name.text.Contains("=") || Name.text.Contains("+") ||
			Name.text.Contains("*") || Name.text.Contains("%") || Name.text.Contains("@") ||
			Name.text.Contains("!"))
		{
			controllerText.text = "Field can contain only letters and hyphens(-)";
		}

		if (Tel.text.Contains("a") || Tel.text.Contains("b") || Tel.text.Contains("c") || Tel.text.Contains("d") ||
			Tel.text.Contains("e") || Tel.text.Contains("f") || Tel.text.Contains("g") || Tel.text.Contains("h") || Tel.text.Contains("i") ||
			Tel.text.Contains("j") || Tel.text.Contains("k") || Tel.text.Contains("l") || Tel.text.Contains("m") || Tel.text.Contains("n") ||
			Tel.text.Contains("o") || Tel.text.Contains("p") || Tel.text.Contains("q") || Tel.text.Contains("r") || Tel.text.Contains("s") ||
			Tel.text.Contains("t") || Tel.text.Contains("u") || Tel.text.Contains("v") || Tel.text.Contains("w") || Tel.text.Contains("x") ||
			Tel.text.Contains("y") || Tel.text.Contains("z"))
		{
			controllerText.text = "Field can contain only numbers";
		}

		if (Tel.text.Contains("A") || Tel.text.Contains("B") || Tel.text.Contains("C") || Tel.text.Contains("D") ||
			Tel.text.Contains("E") || Tel.text.Contains("F") || Tel.text.Contains("G") || Tel.text.Contains("H") || Tel.text.Contains("I") ||
			Tel.text.Contains("J") || Tel.text.Contains("K") || Tel.text.Contains("L") || Tel.text.Contains("M") || Tel.text.Contains("N") ||
			Tel.text.Contains("O") || Tel.text.Contains("P") || Tel.text.Contains("Q") || Tel.text.Contains("R") || Tel.text.Contains("S") ||
			Tel.text.Contains("T") || Tel.text.Contains("U") || Tel.text.Contains("V") || Tel.text.Contains("W") || Tel.text.Contains("X") ||
			Tel.text.Contains("Y") || Tel.text.Contains("Z"))
		{
			controllerText.text = "Field can contain only numbers";
		}

		message = "Name: " + Name.text +
				".     Location: " + Location.text +
				".     Destination: " + Destination.text +
				".     Date and Time: " + DateAndTime.text +
				".     Luggage: " + Luggages.text +
				".     Passengers: " + Number.text +
				".     Phone Number:" + Tel.text + ".";

		message.Replace(".", "\n" + Environment.NewLine);

	}//update

	public void Start()
	{
		message = " Name: " + Name.text + 
			".Location: " + Location.text + 
			".Destination: " + Destination.text + 
			".Date and Time: " + DateAndTime.text + 
			".Luggage: " + Luggages.text +
			".Passengers: " + Number.text +
			".Phone Number:" + Tel.text + ".";

		message.Replace(".", "\n" + Environment.NewLine);

		UnityEngine.Assertions.Assert.IsNotNull(message);

	}//start

	public void SubmitRequest()
	{
		if(Name.text != "" && Location.text != "" && Destination.text != "" && DateAndTime.text != "" &&
			Luggages.text != "" && Number.text != "" && Tel.text != "")
		{
			//do a check to make sure only the right data types are input
			if (Name.text.Contains("0") || Name.text.Contains("1") || Name.text.Contains("2") ||
				Name.text.Contains("3") || Name.text.Contains("4") || Name.text.Contains("5") ||
				Name.text.Contains("6") || Name.text.Contains("7") || Name.text.Contains("8") ||
				Name.text.Contains("9") || Name.text.Contains(".") || Name.text.Contains(",") || Name.text.Contains("/") ||
				Name.text.Contains("?") || Name.text.Contains("=") || Name.text.Contains("+") ||
				Name.text.Contains("*") || Name.text.Contains("%") || Name.text.Contains("@") ||

			    Tel.text.Contains("a") || Tel.text.Contains("b") || Tel.text.Contains("c") || Tel.text.Contains("d") ||
				Tel.text.Contains("e") || Tel.text.Contains("f") || Tel.text.Contains("g") || Tel.text.Contains("h") || Tel.text.Contains("i") ||
				Tel.text.Contains("j") || Tel.text.Contains("k") || Tel.text.Contains("l") || Tel.text.Contains("m") || Tel.text.Contains("n") ||
				Tel.text.Contains("o") || Tel.text.Contains("p") || Tel.text.Contains("q") || Tel.text.Contains("r") || Tel.text.Contains("s") ||
				Tel.text.Contains("t") || Tel.text.Contains("u") || Tel.text.Contains("v") || Tel.text.Contains("w") || Tel.text.Contains("x") ||
				Tel.text.Contains("y") || Tel.text.Contains("z") ||

			    Tel.text.Contains("A") || Tel.text.Contains("B") || Tel.text.Contains("C") || Tel.text.Contains("D") ||
				Tel.text.Contains("E") || Tel.text.Contains("F") || Tel.text.Contains("G") || Tel.text.Contains("H") || Tel.text.Contains("I") ||
				Tel.text.Contains("J") || Tel.text.Contains("K") || Tel.text.Contains("L") || Tel.text.Contains("M") || Tel.text.Contains("N") ||
				Tel.text.Contains("O") || Tel.text.Contains("P") || Tel.text.Contains("Q") || Tel.text.Contains("R") || Tel.text.Contains("S") ||
				Tel.text.Contains("T") || Tel.text.Contains("U") || Tel.text.Contains("V") || Tel.text.Contains("W") || Tel.text.Contains("X") ||
				Tel.text.Contains("Y") || Tel.text.Contains("Z"))
			{
				controllerText.text = "Make Sure You have Inputed the right data";
			}
			else
			{
				string email = reciepient_EmailAddress;
			    string subject = "New Order!";
			    string body =  message;
			    OpenLink("mailto:" + email + "?subject=" + subject + "&body=" + body);
			}

		}
		else
		{
			controllerText.text = "Fill In All Spaces!";
		}

	}//submit request

	public static void OpenLink(string link)
	{
		bool googleSearch = link.Contains("google.com/search");
		string linkNoSpaces = link.Replace(" ", googleSearch ? "+" : "%20");
		Application.OpenURL(linkNoSpaces);

		Debug.Log("Request Sent!");

	}//openlink

	public void CancelRequest()
	{
		sceneChanger.ChangeScene(1);
	}//cancel request


}//class
