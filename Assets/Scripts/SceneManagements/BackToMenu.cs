using Quota.SceneManagement;
using UnityEngine;

public class BackToMenu : MonoBehaviour
{
	SceneChanger sceneChanger;

	private void Awake()
	{
		sceneChanger = FindObjectOfType<SceneChanger>();
	}//Awake
	public void CancelRequest()
	{
		sceneChanger.ChangeScene(1);
	}//cancel request
}//class
