using UnityEngine;
using Quota.SceneManagement;

public class MainMenuFunction : MonoBehaviour
{

	private SceneChanger sceneChanger;

	private void Awake()
	{
		sceneChanger = gameObject.AddComponent<SceneChanger>();
	}//Awake

	public void GetARide()
	{
		sceneChanger.ChangeScene(2);

	}//Get a ride

	public void Help()
	{
		sceneChanger.ChangeScene(3);
	}//Help

	public void AboutUs()
	{
		sceneChanger.ChangeScene(4);
	}//About us

	public void Quit()
	{
		Application.Quit();
	}//Quit
 
}//class
