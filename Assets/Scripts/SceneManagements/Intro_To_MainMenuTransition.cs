using UnityEngine;
using System.Collections;
using Quota.SceneManagement;

public class Intro_To_MainMenuTransition : MonoBehaviour
{
	private SceneChanger sceneChanger;

	public Animator anim;

	private void Awake()
	{
		sceneChanger = FindObjectOfType<SceneChanger>();

		DontDestroyOnLoad(this.gameObject);
		StartCoroutine(Sequence());
		
	}//awake

	private IEnumerator Sequence()
	{
		anim.SetTrigger("fade");
		yield return new WaitForSeconds(2.8f);
		sceneChanger.ChangeScene(1);

	}//Sequence

}//class
