using UnityEngine;
using UnityEngine.SceneManagement;

namespace Quota.SceneManagement
{
	public class SceneChanger : MonoBehaviour
	{
		public void ChangeScene(int index)
		{
			SceneManager.LoadScene(index);

		}//Change Scene

	}//class
}//namespace
